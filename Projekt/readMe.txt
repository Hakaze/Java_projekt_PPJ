Dodatkowy projekt na ocen� 5 wykonany na przedmiocie Podstawy programowania w Javie na I semestrze.
Tre�� zadania znajdue si� w pliku "PPJ projekt.pdf", natomiast do jego uruchomienia wymagane s� pliki glowa i glowa.color
Program wczytuje zapisany tr�jwymiarowy skan g�owy pewnej osoby i wy�wietla go w 3D z mo�liwo�ci� obracania.
Projekt nie jest doskona�y zwa�ywszy, i� by� wykonywany na I semestrze, jednak�e zawiera du�o ciekawych element�w. Mi�dzy
innymi ca�a obs�uga 3D zosta�a r�cznie zaimplementowana przy pomocy biblioteki do tworzenia grafiki 2D Awt, ponadto
wczytywanie plik�w �r�d�owych r�znie� zosta�o zrealizowane od zera ( w tre�ci zadania nie by�y podane �adne inforamcje na
ich temat )
Projekt spotka� si� z uznaniem i pozwoli� zaliczy� �wiczenia z ocen� 5 :)