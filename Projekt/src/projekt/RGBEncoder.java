package projekt;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class RGBEncoder {

	boolean isMagic;
	boolean isCompressed;
	byte BPC;
	byte dimension;
	int xSize, ySize;
	int channels;
	long colorMap;
	long pimin;
	long pimax;
	long pSize;

	int[][][] scanlines = new int[3][512][450];

	public RGBEncoder() {

	}

	public void Load(String adres) {
		try {
			InputStream is = new FileInputStream(adres);
			readHeader(is);

			byte[][] channelsShifts = { {}, { 0 }, {}, { 16, 8, 0 }, { 16, 8, 0, 24 } };

			int tmp = 0;
			byte[] startTableB = null;
			byte[] lengthTableB = null;
			if (isCompressed) {
				tmp = 4 * ySize * channels;
				startTableB = new byte[tmp];
				lengthTableB = new byte[tmp];
				is.read(startTableB);
				is.read(lengthTableB);
			}
			tmp = tmp * 2 + 512;
			byte[] all = new byte[is.available()];
			is.read(all);

			int[] lineData = new int[xSize];
			int scanline = ySize - 1;
			byte channel;
			boolean reset;

			while (scanline >= 0) {
				channel = (byte) (channels - 1);
				reset = true;
				while (channel >= 0) {
					decodeRLE(all, (int) getLong(startTableB, (scanline + channel * ySize) * 4) - tmp,
							(int) getLong(lengthTableB, (scanline + channel * ySize) * 4),
							channelsShifts[channels][channel], lineData, reset, scanline, channel);
					channel--;
					reset = false;

				}

				for (int i = 0; i < 450; i++) {

					scanlines[0][scanline][i] = (lineData[i] >>> 16) & (0xff);
					scanlines[1][scanline][i] = (lineData[i] >>> 8) & (0xff);
					scanlines[2][scanline][i] = (lineData[i]) & (0xff);
				}

				scanline--;

			}
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	void readHeader(InputStream fis) {
		byte[] headerR = new byte[512];

		try {
			fis.read(headerR);
			int count = 0;
			isMagic = (getShort(headerR, count) == 474);
			count += 2;
			isCompressed = (getByte(headerR, count++) == 1);
			BPC = (byte) getByte(headerR, count++);
			dimension = (byte) getShort(headerR, count);
			count += 2;
			xSize = getShort(headerR, count);
			ySize = getShort(headerR, count + 2);
			channels = getShort(headerR, count + 4);
			count += 6;
			pimin = getLong(headerR, count);
			pimax = getLong(headerR, count + 4);
			pSize = pimax - pimin;
			count += 84;

			colorMap = getLong(headerR, count);
			headerR = null;
		} catch (IOException e) {

		}

	}

	long getLong(byte[] mb, int offset) {
		int[] tmpA = new int[4];
		tmpA[0] = 0xFF & (int) mb[offset];
		tmpA[1] = 0xFF & (int) mb[offset + 1];
		tmpA[2] = 0xFF & (int) mb[offset + 2];
		tmpA[3] = 0xFF & (int) mb[offset + 3];

		long l2 = ((long) (tmpA[0] << 24 | tmpA[1] << 16 | tmpA[2] << 8 | tmpA[3]));
		return l2;
	}

	int getShort(byte[] mb, int offset) {
		int[] tmpA = new int[4];
		tmpA[0] = 0xFF & (int) mb[offset];
		tmpA[1] = 0xFF & (int) mb[offset + 1];

		int l1 = (int) (tmpA[0] << 8 | tmpA[1]);
		return l1;
	}

	private int getByte(byte[] mb, int offset) {
		return 0xFF & (int) mb[offset];
	}

	void decodeRLE(byte[] allData, int start, int len, byte channelShift, int[] result, boolean reset, int scanline,
			byte channel) {

		int max = start + len;
		int rCounter = 0;
		int count;

		if (!reset) {
			while (start < max) {
				int tmp = allData[start++] & 0xff;

				if ((count = (int) tmp & 0x7f) == 0)
					return;

				if ((tmp & 0x80) == 0x80) {

					while (count != 0) {

						tmp = (int) allData[start++] & 0xff;

						tmp = tmp << channelShift;
						result[rCounter] = result[rCounter] | (int) tmp;
						rCounter++;
						count--;
					}
				} else {

					tmp = (int) allData[start++] & 0xff;
					tmp = tmp << channelShift;
					while (count != 0) {

						result[rCounter] = result[rCounter] | (int) tmp;
						rCounter++;
						count--;
					}
				}
			}
		} else {
			while (start < max) {
				int tmp = allData[start++] & 0xff;

				if ((count = (int) tmp & 0x7f) == 0)
					return;

				if ((tmp & 0x80) == 0x80) {

					while (count != 0) {

						tmp = (int) allData[start++] & 0xff;

						tmp = tmp << channelShift;
						result[rCounter] = channelShift > 16 ? (int) tmp : (int) tmp | 0xff000000;
						rCounter++;
						count--;
					}
				} else {

					tmp = (int) allData[start++] & 0xff;
					tmp = tmp << channelShift;
					while (count != 0) {

						result[rCounter] = channelShift > 16 ? (int) tmp : (int) tmp | 0xff000000;
						rCounter++;
						count--;
					}
				}
			}
		}

		return;

	}

}
