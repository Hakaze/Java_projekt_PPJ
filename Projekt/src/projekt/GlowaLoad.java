package projekt;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class GlowaLoad {
	int nlt = 512;
	int nlg = 450;
	long[][] data = new long[nlt][nlg];
	short promien_packed;
	long promien_um;
	int rshift = 4;

	public void Load(String adres) {
		try {
			DataInputStream strumien = new DataInputStream(new FileInputStream(adres));
			strumien.skipBytes(241);
			for (int i = 0; i < nlt; i++) {
				for (int j = 0; j < nlg; j++) {
					promien_packed = strumien.readShort();
					promien_packed = (short) (promien_packed << 8 | promien_packed >> 8);
					promien_um = promien_packed << rshift;
					if (promien_um > 0) {
						data[i][j] = promien_um;
						if (promien_um > 524272) {
							System.out.println("Nieprawidlowa wielkosc");
						}
					} else {
						data[i][j] = -1;

					}
				}
			}
			strumien.close();

		} catch (FileNotFoundException e) {
			System.out.println("Nie znaleziono pliku");
		} catch (IOException e) {
			System.out.println("B��d wej�cia-wyj�cia");
		}
	}
}
