package projekt;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.GeneralPath;

public class Window extends Frame implements MouseListener, MouseMotionListener, KeyListener, MouseWheelListener {

	int xx;
	int yy;
	double posX = 0;
	double negX = 0;
	double posY = 0;
	double negY = 0;
	int nlt = 450;
	int nlg = 512;
	double lgDistance = 0.012272;
	int deltaZ = 700;
	double centrX = 0;
	double centrY = 0;

	Point3D[][] p = new Point3D[nlg][nlt];
	boolean[][] czyIstnieje = new boolean[nlg][nlt];
	Point2D[][] pp = new Point2D[nlg][nlt];

	double angle = 30 * (Math.PI / 360);
	double angleZETA = 0;
	boolean first = true;

	RotationMatrix RotMat;
	RGBEncoder R;
	int height = java.awt.Toolkit.getDefaultToolkit().getScreenSize().height;
	int width = java.awt.Toolkit.getDefaultToolkit().getScreenSize().width;

	double scale = (double) height / ((double) nlt * (double) deltaZ);;

	public Window(String filenameGlowa, String filenameGlowaColor) {

		R = new RGBEncoder();
		R.Load(filenameGlowaColor);

		GlowaLoad Dane = new GlowaLoad();
		Dane.Load(filenameGlowa);

		double srednia = 0;
		int licznik = 0;
		for (int i = 0; i < nlg; i++) {
			srednia = 0;
			licznik = 0;

			for (int j = 0; j < nlt; j++) {

				double[] tab = convert(Dane.data[i][j], j, i);
				if (tab[2] != -1) {
					srednia += Dane.data[i][j];
					licznik++;
					p[i][j] = new Point3D(tab[0], tab[1], tab[2]);
					pp[i][j] = new Point2D(303, 303);
					czyIstnieje[i][j] = true;
				} else {
					p[i][j] = new Point3D(-1, -1, -1);
					pp[i][j] = new Point2D(-1, -1);
					czyIstnieje[i][j] = false;
				}
			}

			srednia = srednia / licznik;
			for (int j = 0; j < nlt; j++) {
				if (Dane.data[i][j] > 1.45 * srednia) {
					czyIstnieje[i][j] = false;
				}
			}

		}

		RotMat = new RotationMatrix(angle, angleZETA);
		setLayout(new FlowLayout());
		addMouseListener(this);
		addMouseMotionListener(this);
		addKeyListener(this);
		addMouseWheelListener(this);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				System.exit(0);
			}
		});
		setSize(width, height);
		setVisible(true);
		CreatePoints();
		Centr();

	}

	public double[] convert(long r, int z, int ltNumber) {
		double[] tab = new double[3];
		if (r != -1) {
			tab[0] = (r * Math.cos(ltNumber * lgDistance));
			tab[1] = (r * Math.sin(ltNumber * lgDistance));
			tab[2] = z * deltaZ;

			return tab;
		}
		tab[0] = -1;
		tab[1] = -1;
		tab[2] = -1;
		return tab;

	}

	public void CreatePoints() {
		RotMat.update(angle, angleZETA);
		boolean centrFirst = true;
		for (int i = 0; i < nlg; i++) {
			for (int j = 0; j < nlt; j++) {
				if (czyIstnieje[i][j]) {
					double[] v = RotMat.multiplied(p[i][j]);

					pp[i][j].setY((-v[0] + centrY) * scale);
					pp[i][j].setX((-v[1] + centrX) * scale);

					if (centrFirst) {
						posX = pp[i][j].getX();
						negX = pp[i][j].getX();
						posY = pp[i][j].getY();
						negY = pp[i][j].getY();
						centrFirst = false;
					}
					if (pp[i][j].getX() > posX) {
						posX = pp[i][j].getX();
					} else if (pp[i][j].getX() < negX) {
						negX = pp[i][j].getX();
					}
					if (pp[i][j].getY() > posY) {
						posY = pp[i][j].getY();
					} else if (pp[i][j].getY() < negY) {
						negY = pp[i][j].getY();
					}
				}
			}
		}
	}

	public void paint(Graphics g) {

		Graphics2D g2 = (Graphics2D) g;
		CreatePoints();

		for (int i = 0; i < nlt - 1; i++) {
			for (int j = 0; j < nlg; j++) {

				if (czyIstnieje[j][i] && czyIstnieje[j][i + 1]) {
					int jj = FindNextTwoPoints(i, j);

					GeneralPath polygon = new GeneralPath(GeneralPath.WIND_EVEN_ODD, 4);

					double[] pointsX = { pp[j][i].getX(), pp[j][i + 1].getX(), pp[jj][i + 1].getX(), pp[jj][i].getX() };
					double[] pointsY = { pp[j][i].getY(), pp[j][i + 1].getY(), pp[jj][i + 1].getY(), pp[jj][i].getY() };
					polygon.moveTo(pointsX[0], pointsY[0]);
					for (int k = 1; k < 4; k++) {
						polygon.lineTo(pointsX[k], pointsY[k]);
					}
					polygon.closePath();
					Color c;
					try {
						c = new Color(R.scanlines[0][j][i], R.scanlines[1][j][i], R.scanlines[2][j][i]);
					} catch (IllegalArgumentException e) {
						c = Color.BLACK;
					}
					g2.setColor(c);
					g2.fill(polygon);
					g2.draw(polygon);

					if (jj < j)
						j = nlg;

				}

			}
		}

	}

	public int FindNextTwoPoints(int i, int j) {
		if (j == nlg - 1) {
			j = 0;
		} else {
			j++;
		}
		while (!(czyIstnieje[j][i] && czyIstnieje[j][i + 1])) {
			j++;
			if (j >= nlg - 1) {
				j = 0;
			}

		}

		return j;
	}

	public void Centr() {
		double srX = (posX + negX) / 2;
		double srY = (posY + negY) / 2;
		centrX = (this.getWidth() / 2 - srX) * 1 / scale;
		centrY = (this.getHeight() / 2 - srY) * 1 / scale;

	}

	public void mouseClicked(MouseEvent e) {

	}

	public void mousePressed(MouseEvent e) {

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		first = true;
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mouseMoved(MouseEvent e) {

	}

	@Override
	public void mouseDragged(MouseEvent e) {

		if (first) {
			xx = e.getX();
			yy = e.getY();
			first = false;
		}

		angle = angle + (yy - e.getY()) * (Math.PI / 360);

		if (angle >= 2 * Math.PI) {
			angle = angle - 2 * Math.PI;
		}
		if (angle <= 2 * Math.PI) {
			angle = angle + 2 * Math.PI;
		}

		angleZETA = angleZETA + (xx - e.getX()) * (Math.PI / 360);

		if (angleZETA >= 2 * Math.PI) {
			angleZETA = angleZETA - 2 * Math.PI;
		}
		if (angleZETA <= 2 * Math.PI) {
			angleZETA = angleZETA + 2 * Math.PI;
		}
		repaint();
		xx = e.getX();
		yy = e.getY();

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent e) {
		int keyCode = e.getKeyCode();
		int przesuniecie = 1000;
		switch (keyCode) {
		case KeyEvent.VK_UP:
			centrY = centrY - przesuniecie;
			break;
		case KeyEvent.VK_DOWN:
			centrY = centrY + przesuniecie;
			break;
		case KeyEvent.VK_LEFT:
			centrX = centrX - przesuniecie;
			break;
		case KeyEvent.VK_RIGHT:
			centrX = centrX + przesuniecie;
			break;
		}
		repaint();
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	public void mouseWheelMoved(MouseWheelEvent e) {
		if (e.getWheelRotation() > 0) {
			scale = scale + 0.0001;
			CreatePoints();
			repaint();
		} else if (e.getWheelRotation() < 0) {
			scale = scale - 0.0001;
			CreatePoints();
			repaint();
		}

	}

}
