package projekt;

public class RotationMatrix {
	double fi;
	double zeta;
	double[][] matrix = new double[3][3];

	public RotationMatrix(double fi, double zeta) {
		update(fi, zeta);
	}

	public double[] multiplied(Point3D p) {
		double[] point = new double[3];
		point[0] = p.getX();
		point[1] = p.getY();
		point[2] = p.getZ();
		double[] vector = new double[3];

		vector[0] = point[0] * matrix[0][0] + point[1] * matrix[0][1] + point[2] * matrix[0][2];
		vector[1] = point[0] * matrix[1][0] + point[1] * matrix[1][1] + point[2] * matrix[1][2];
		vector[2] = point[0] * matrix[2][0] + point[1] * matrix[2][1] + point[2] * matrix[2][2];
		return vector;
	}

	public void update(double fi, double zeta) {
		this.fi = fi;
		double CosFi = Math.cos(fi);
		double SinFi = Math.sin(fi);
		this.zeta = zeta;
		double CosZeta = Math.cos(zeta);
		double SinZeta = Math.sin(zeta);

		matrix[0][0] = CosZeta * SinFi;
		matrix[0][1] = SinZeta * SinFi;
		matrix[0][2] = CosFi;

		matrix[1][0] = -SinZeta;
		matrix[1][1] = CosZeta;
		matrix[1][2] = 0;

		matrix[2][0] = -CosZeta * CosFi;
		matrix[2][1] = -SinZeta * CosFi;
		matrix[2][2] = SinFi;
	}

}
