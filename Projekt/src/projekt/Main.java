// Micha� Jakubowicz s13589
package projekt;

import java.awt.FileDialog;
import java.awt.Frame;

//W pierwszym okienku nale�y wybra� plik 'glowa', natomiast w drugim okienku plik 'glowa.color'.
//Obs�uga przy pomocy myszy, strza�kami mo�na przesuwa� obraz a k�kiem myszy mo�na regulowa� powi�kszenie.

public class Main {

	public static void main(String[] args) {
		Frame f1 = new Frame();

		FileDialog Glowa = new FileDialog(f1, "Wybierz plik 'glowa'", FileDialog.LOAD);
		Glowa.setDirectory("C:\\");
		Glowa.setVisible(true);
		String filenameGlowa = Glowa.getDirectory();

		filenameGlowa = filenameGlowa + Glowa.getFile();

		FileDialog Color = new FileDialog(f1, "Wybierz plik 'glowa.color'", FileDialog.LOAD);
		Color.setDirectory("C:\\");
		Color.setVisible(true);
		String filenameGlowaColor = Color.getDirectory();

		filenameGlowaColor = filenameGlowaColor + Color.getFile();

		String poprawnyPlikG = "glowa";
		String poprawnyPlikGC = "glowa.color";

		if (filenameGlowa.contains(poprawnyPlikG) && filenameGlowaColor.contains(poprawnyPlikGC)) {
			Window window = new Window(filenameGlowa, filenameGlowaColor);
		} else {
			System.out.println("Wybrane pliki nie s� poprawne");
		}

	}

}
